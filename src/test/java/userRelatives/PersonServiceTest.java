package userRelatives;

import static org.junit.Assert.*;

import java.util.Date;

import lt.alna.entities.Person;
import lt.alna.services.PersonService;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/root-context.xml" })
public class PersonServiceTest {
	
	@Autowired
	private PersonService service;
	
	@Test
	public void findAllPersonsTest(){//by default there are 7 persons in DB
		assertEquals("Incorrect amount of persons in database.", 7, service.findAll().size());
	}
	
	@Test
	public void createNewPersonTest(){
		service.create(new Person("Tadas", "Tadaitis", new Date()));
		assertEquals("New person not created", 8, service.findById(8).getId());
	}
	
	@Test
	public void deletePersonTest(){
		service.delete(4);
		assertEquals("Person was not deleted.", 7, service.findAll().size());
	}
	
	@Test
	public void findPersonRelativesTest(){
		assertEquals("Incorrect amount of 'Jonaitis' relatives found.", 4, service.findRelativesBy("Jonait").size());
	}
	
	@Test
	public void updateTest(){
		Person oldPerson = service.findById(5);
		oldPerson.setName("Testas");
		oldPerson.setSurname("Testininkas");
		
		service.update(oldPerson);
		
		Person updatedPerson = service.findById(5);
		assertEquals("Incorrect surname of updated person.", "Testininkas", updatedPerson.getSurname());
		assertEquals("Incorrect name of updated person.", "Testas", updatedPerson.getName());
	}

}

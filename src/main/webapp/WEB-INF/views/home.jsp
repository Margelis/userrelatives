<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home Page</title>

	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	<link href="<c:url value="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />" rel="stylesheet">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.0.min.js"></script> 
	<script type="text/javascript" src="http://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script> 

</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-lg-4">
			<h1>
				Add/Edit a Person
			</h1>
		
			<c:url var="addAction" value="/add" ></c:url>
		
			<form:form action="${addAction}" commandName="person">
			<div class="form-group">
				<table>
					<tbody>
					<c:if test="${!empty person.name}">
					<tr>
						<td>
							<form:label path="id">
								<spring:message text="ID"/>
							</form:label>
						</td>
						<td>
							<form:input path="id" readonly="true"  class="form-control" size="8"  disabled="true" />
							<form:hidden path="id" />
						</td> 
					</tr>
					<br/>
					</c:if>
					</tbody>
					<tr>
						<td>
							<form:label path="Name">
								<spring:message text="Name"/>
							</form:label>
						</td>
						<td>
							<form:input path="name"  class="form-control" required="required" />
						</td> 
					</tr>
					
					<tr>
						<td>
							<form:label path="Surname">
								<spring:message text="Surname"/>
							</form:label>
						</td>
						<td>
							<form:input path="surname"  class="form-control" required="required"/>
						</td>
					</tr>
					
					<tr>
						<td>
							<form:label path="Birthday">
								<spring:message text="Birthday"/>
							</form:label>
						</td>
						<td>
							<form:input path="birthday"  class="form-control" type="date" required="required"/>
						</td>
					</tr>
					
					<tr>
						<td colspan="2">
							<c:if test="${!empty person.name}">
								<input type="submit"  class="btn btn-primary col-lg-offset-6"
									value="<spring:message text="Edit Person"/>" />
							</c:if>
							<c:if test="${empty person.name}">
								<input type="submit"  class="btn btn-primary col-lg-offset-6"
									value="<spring:message text="Add Person"/>" />
							</c:if>
						</td>
					</tr>
				</table>
				</div>	
			</form:form>
		</div>
		
		<div class="col-lg-6">
		
		<h1>Person Relatives</h1>
		
			<table class="tableStyle">
			<c:if test="${!empty findPersonRelatives}">
				<thead>
					<tr>
					<th width="120">Name</th>
					<th width="120">Surname</th>
					<th width="120">Birthday</th>
					</tr>
				</thead>	
			</c:if>
			<tbody>
				<c:forEach var="user" items="${findPersonRelatives}">
					
					<tr>
						<td>
							<c:out value="${user.name}"/>
						</td>
						<td>
							<c:out value="${user.surname}"/>
						</td>
						<td>
							<c:out value="${user.formattedBirthday}"/>
						</td>
					</tr>
					
				</c:forEach>
			</tbody>
				<c:if test="${empty findPersonRelatives}">
					no user selected.
				</c:if>
			</table>
		</div>
		
	</div>
	<br>
	<h3>Persons List</h3>
	<c:if test="${!empty findAll}">
		<table class="tableStyle sortable">
			<thead>
				<tr>
					<th width="120">Name</th>
					<th width="120">Surname</th>
					<th width="120">Birthday</th>
					<th width="60" class="sorttable_nosort">Edit</th>
					<th width="60" class="sorttable_nosort">Delete</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${findAll}" var="person">
					<tr>
						<td>${person.name}</td>
						<td>${person.surname}</td>
						<td>${person.formattedBirthday}</td>
						<td><a href="<c:url value='/edit/${person.id}' />" >Edit</a></td>
						<td><a href="<c:url value='/remove/${person.id}' />" >Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
</div>
<br/>
</body>
</html>

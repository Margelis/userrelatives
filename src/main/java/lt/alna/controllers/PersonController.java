package lt.alna.controllers;

import lt.alna.entities.Person;
import lt.alna.services.PersonService;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class PersonController {
	private static final Logger logger = LoggerFactory
			.logger(PersonController.class);
	@Autowired
	private PersonService personService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("person", new Person());
		model.addAttribute("findAll", this.personService.findAll());
		return "home";
	}

	// For add and update person both
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("person") Person p) {
		if (p.getId() == 0) {
			// new person, add it
			this.personService.create(p);
		} else {
			// existing person, call update
			this.personService.update(p);
		}

		return "redirect:/";
	}

	@RequestMapping("/remove/{id}")
	public String removePerson(@PathVariable("id") int id) {
		this.personService.delete(id);
		return "redirect:/";
	}

	@RequestMapping("/edit/{id}")
	public String editPerson(@PathVariable("id") int id, Model model) {
		Person person = this.personService.findById(id);
		model.addAttribute("person", person);
		model.addAttribute("findAll", this.personService.findAll());
		model.addAttribute("findPersonRelatives", this.personService
				.findRelativesBy(findSurnamePattern(person.getSurname())));
		return "home";
	}

	/**
	 * Finds surname root pattern excluding Lithuanian endings and without
	 * Lithuanian symbols. If surname doesn't have Lithuanian ending, full
	 * surname is returning.
	 * 
	 * @param surname
	 *            to find root pattern
	 * @return surname pattern
	 */
	private String findSurnamePattern(String surname) {
		String surnamePattern = "";
		String[] surnameEndsArray = { "iene", "yte", "aite", "is", "ius",
				"iute", "as", "e", "a" };

		for (String endString : surnameEndsArray) {

			if (surname.endsWith(endString)) {
				surnamePattern = surname.substring(0, surname.length()
						- endString.length());
				logger.info("Found surname end: " + endString
						+ ". surname pattern: " + surnamePattern);
				return surnamePattern;
			}
		}
		
		return surname;
	}

	public void setPersonService(PersonService ps) {
		this.personService = ps;
	}

}

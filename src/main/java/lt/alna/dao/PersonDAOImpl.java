package lt.alna.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import lt.alna.entities.Person;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PersonDAOImpl implements PersonDAO {

	private static final Logger logger = LoggerFactory
			.logger(PersonDAOImpl.class);

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void create(Person person) {
		em.persist(person);
		logger.info("Person saved successfully, Person Details=" + person);
	}
	
	@Transactional
	public void update(Person p) {
		Query clientQuery = em
				.createQuery("UPDATE Person p set p.name =:name, p.surname =:surname, p.birthday =:birthday WHERE p.id =:id");
		clientQuery.setParameter("name", p.getName());
		clientQuery.setParameter("surname", p.getSurname());
		clientQuery.setParameter("birthday", p.getBirthday());
		clientQuery.setParameter("id", p.getId());
		clientQuery.executeUpdate();
		logger.info("Person updated successfully, Person Details=" + p);
	}

	public List<Person> findRelativesBy(String surnamePattern) {
		TypedQuery<Person> query = em.createQuery(
				"SELECT p From Person p WHERE p.surname LIKE :surnamePattern",
				Person.class);
		query.setParameter("surnamePattern", surnamePattern + "%");
		return query.getResultList();
	}

	public List<Person> findAll() {
		TypedQuery<Person> query = em.createQuery(
				"SELECT p FROM Person p ORDER BY p.id", Person.class);
		return query.getResultList();
	}

	public Person findById(int id) {
		Person p = em.getReference(Person.class, id);
		logger.info("Person loaded successfully, Person details=" + p);
		return em.getReference(Person.class, id);
	}

	@Transactional
	public void delete(int userId) {
		Person person = em.getReference(Person.class, userId);
		em.remove(person);
		logger.info("Person deleted successfully, person details=" + person);
	}

}

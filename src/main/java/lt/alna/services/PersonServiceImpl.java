package lt.alna.services;

import java.util.List;

import lt.alna.dao.PersonDAO;
import lt.alna.entities.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonDAO personDAO;

	@Transactional
	public void create(Person p) {
		this.personDAO.create(p);
	}

	@Transactional
	public void update(Person p) {
		this.personDAO.update(p);
	}

	@Transactional
	public List<Person> findAll() {
		return this.personDAO.findAll();
	}

	@Transactional
	public Person findById(int id) {
		return this.personDAO.findById(id);
	}

	@Transactional
	public void delete(int id) {
		this.personDAO.delete(id);
	}
	
	@Transactional
	public List<Person> findRelativesBy(String surnamePattern) {
		return this.personDAO.findRelativesBy(surnamePattern);
	}

}

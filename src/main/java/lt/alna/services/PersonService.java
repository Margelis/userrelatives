package lt.alna.services;

import java.util.List;

import lt.alna.entities.Person;

public interface PersonService {

	public void create(Person p);

	public void update(Person p);

	public List<Person> findAll();

	public Person findById(int id);

	public void delete(int id);

	public List<Person> findRelativesBy(String surnamePattern);

}
